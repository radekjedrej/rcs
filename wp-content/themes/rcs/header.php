<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and <header>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body>

	<?php 

	$info = get_field("info_row", "option");

	?>
	<header class="nav-bar">
		
		<!-- Desktop Nav Below -->

		<div class="nav-bar__desktop-nav d-desktop">
			<div class="nav-bar__border-inner">
				<div class="nav-bar__nav wrapper-full d-flex">
					<div class="nav-bar__inner d-flex">
						<div class="nav-bar__logo d-flex">
							<img src="<?= get_template_directory_uri()?>/src/images/TheViewLogo.png" alt="Logo">
						</div>
						<div class="nav-bar__content d-flex">
							<?php if($info):
								
								$number = $info["number"] ? $info["number"] : "";
								$email = $info["email"] ? $info["email"] : "";
								$buttonLink = $info["button_link"] ? $info["button_link"] : "";
	
								?>
								<div class="nav-bar__info-row d-flex">
									<ul class="nav-bar__info-list d-flex">
										<li class="nav-bar__info-item text-button-regular"><?= $number ?></li>
										<li class="nav-bar__info-item text-button-regular"><?= $email ?></li>
										<li class="nav-bar__info-item">
											<a class="nav-bar__button u-btn u-btn--nav text-button-regular" href="<?= $buttonLink["url"] ?>"><?= $buttonLink["title"] ?></a>
										</li>
									</ul>
								</div>
							<?php endif; ?>
	
							<?php if(have_rows("menu_links", "option")): ?>
								<div class="nav-bar__links-row d-flex">
									<ul class="nav-bar__links-list d-flex">
										<?php while(have_rows("menu_links", "option")): the_row(); 
										
											$link = get_sub_field("link") ?  get_sub_field("link") : "";
											$fullWidth = get_sub_field("full_width");
										
										?>
										<li class="nav-bar__links-item <?=($fullWidth) ? "": "nav-bar__links-item--relative" ?>">
											<a class="nav-bar__link text-button-regular" href="<?= $link["url"] ?>"><?= $link["title"] ?></a>
											
											<?php
											if(have_rows("sub_menu_column", "option")):
												
												if(!$fullWidth): ?>
													<ul class="nav-bar__link-dropdown-small">
													<?php while(have_rows("sub_menu_column", "option")): the_row(); ?>
							
														<?php while(have_rows("sub_menu_links", "option")): the_row();
															
															$link = get_sub_field("link") ? get_sub_field("link") : "";
															$bold = get_sub_field("link_with_bold_font") ? get_sub_field("link_with_bold_font") : "";
	
														?>
															<?php if($link): ?>
																<li class="nav-bar__drop-link color-white <?=($bold) ? "text-button-bold" : "text-button-regular" ?>"><a href="<?= $link['url'] ?>"><?= $link['title']?></a></li>
															<?php endif; ?>
														<?php endwhile; ?>
													<?php endwhile; ?>
													</ul>
	
													<?php else: ?>
													
													<div class="nav-bar__link-dropdown-full">
														<ul class="nav-bar__link-dropdown-full__ul">
														<?php while(have_rows("sub_menu_column", "option")): the_row(); 
		
															$title = get_sub_field("title") ?  get_sub_field("title") : ""; ?>
		
															<ul class="nav-bar__link-dropdown-full-column">
															
															<?php if($title): ?>
																<li class="nav-bar__link-dropdown-title color-white text-button-bold"><?= $title ?></li>
															<?php endif; ?>
		
															<?php while(have_rows("sub_menu_links", "option")): the_row();
																
																$link = get_sub_field("link") ? get_sub_field("link") : "";
																$bold = get_sub_field("link_with_bold_font") ? get_sub_field("link_with_bold_font") : "";
		
															?>
															
															<?php if($link): ?>
																	<li class="nav-bar__drop-link color-white <?=($bold) ? "text-button-bold" : "text-button-regular" ?>"><a href="<?= $link['url'] ?>"><?= $link['title']?></a></li>
																<?php endif; ?>
															<?php endwhile; ?>
															</ul>
														<?php endwhile; ?>
														</ul>
													</div>
												<?php endif; ?>
											<?php endif; ?>
										</li>
										<?php endwhile; ?>
									</ul>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Mobile Nav Below -->

		<div class="nav-bar__mobile-nav d-mobile">
			<div class="nav-bar__mobile-inner wrapper-full d-flex">
				<div class="nav-bar__mobile-logo d-flex">
					<img src="<?= get_template_directory_uri()?>/src/images/TheViewLogo.png" alt="Logo">
				</div>
					
				<div class="nav-bar__burger-icon">
					<div class="nav-bar__burger-icon-line"></div>
				</div>									
			</div>
		</div>
		<div class="nav-bar__mobile-dropdown">
			<div class="nav-bar__mobile-dropdown-content wrapper-stretched d-flex">
				<div class="nav-bar__dropdown-button-container">
					<a class="nav-bar__dropdown-button u-btn text-button-regular" href="#">ENQUIRE</a>											
				</div>
				<?php if(have_rows("menu_links", "option")): ?>
					<ul class="nav-bar__dropdown-list">
						<?php while(have_rows("menu_links", "option")): the_row(); 
										
							$link = get_sub_field("link") ?  get_sub_field("link") : "";
									
						?>
							<li class="nav-bar__dropdown-list-item">
								<div class="nav-bar__dropdown-list-item-border">
									<div class="nav-bar__dropdown-list-item-inner">
										<a href="<?= $link['url'] ?>" class="nav-bar__dropdown-item-link d-flex">
											<span class="text-button-regular"><?= $link['title'] ?></span>
											<?php include get_icons_directory("chevron-down.svg") ?>
										</a>
									</div>
								</div>
								<ul class="nav-bar__second-dropdown">
								<?php while(have_rows("sub_menu_column", "option")): the_row(); 
	
									$title = get_sub_field("title") ?  get_sub_field("title") : ""; 
									
									?>

									<?php if($title): ?>
											<li class="nav-bar__second-dropdown-item text-button-regular"><?= $title ?></li>
									<?php endif; ?>

									<?php while(have_rows("sub_menu_links", "option")): the_row();
															
										$link = get_sub_field("link") ? get_sub_field("link") : "";
										$bold = get_sub_field("link_with_bold_font") ? get_sub_field("link_with_bold_font") : "";
	
										?>
										
										<?php if($link): ?>
											<li class="nav-bar__second-dropdown-item text-button-regular">
												<a href="#">
													<?= $link['title'] ?>
												</a>
											</li>
										<?php endif; ?>
									<?php endwhile; ?>
								<?php endwhile; ?>
								</ul>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</header>


	
