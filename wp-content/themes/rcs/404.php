<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="main-wrapper">
		<?php 

		$image = get_field("404_image", "option");

		if($image):
			$picture = $image['url'];
		else:
			$picture = "";
		endif;

		?>

			<section class="error-404 wrapper-stretched not-found">
				<div class="error-404__container d-flex">
					<div class="error-404__space-left"></div>
					<div class="error-404__img-container">
						<img class="error-404__img" src="<?= $picture ?>" alt="404 Image">
					</div>
				</div>

				<div class="error-404__info">
					<div class="error-404__info-inner">
						<h1 class="error-404__error-code text-subheader">404</h1>
						<h2 class="error-404__error-description text-header"><?php esc_html_e( 'Sorry we can&rsquo;t find that page.', '_s' );?></h2>
						<p class="error-404__copy text-regular">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<a class="error-404__button u-btn" href="<?= get_home_url() ?>">
                            <div class="u-btn__hover-div"></div>
                            <span class="u-btn__text text-button-regular">Return Home</span>
                        </a>   
					</div>
				</div>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
