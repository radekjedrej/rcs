<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

<footer class="footer">
    <div class="footer__bg">
        <div class="footer__container wrapper-full d-flex d-flex-wrap">
            <div class="footer__sign-up wrapper-small d-flex d-flex-wrap">
                <div class="footer__sign-up-inner">
                    <div class="footer__newsletter-info">
                        <h2 class="footer__description text-footer color-white">Sign up to the newsletter and be the first to hear about our latest offers.</h2>
                    </div>
                    <div class="footer__newsletter-form d-flex d-flex-wrap">
                        <form class="footer__form d-flex d-flex-wrap">
                            <input class="footer__email text-button-regular" type="email" placeholder="Email Address">
                            <input class="footer__name text-button-regular" type="text" placeholder="Full Name">
                            <input class="footer__submit text-button-regular color-white" type="submit" value="Sign Up">    
                        </form>
                    </div>
                </div>
            </div>

            <?php if(have_rows("footer_links", "option")): ?>
                <div class="footer__columns-row d-flex">
                <?php while(have_rows("footer_links", "option")): the_row(); 
                
                    $mainLink = get_sub_field("link") ?  get_sub_field("link") : "";
                
                    ?>
                    <div class="footer__column">
                        <div class="footer__column-title-container">
                            <h2 class="footer__column-title text-footer color-white"><?= $mainLink["title"] ?></h2>
                        </div>
                        <?php if(have_rows("sub_menu", "option")): ?>
                            <div class="footer__links">
                                <ul class="footer__list">
                                <?php while(have_rows("sub_menu", "option")): the_row(); 
                                
                                    $linkType = get_sub_field("link_type") ? get_sub_field("link_type") : "";
                                    $link = get_sub_field("link") ? get_sub_field("link") : "";

                                    ?>
                                    
                                    <?php if($linkType == 'link'): ?>
                                        <li class="footer__list-item text-regular">
                                            <a href="#"><?= $link["title"] ?></a>
                                        </li>
                                    <?php 
                                
                                    else: ?>

                                        <?php if(have_rows("socials", "option")): ?>
                                            <li class="footer__socials d-flex">
                                            <?php while(have_rows("socials", "option")): the_row(); 
                                            
                                                $socialIcon = get_sub_field("social_icon") ? get_sub_field("social_icon") : "";
                                                $socialIconLink = get_sub_field("social_icon_link") ? get_sub_field("social_icon_link") : "";
                                            
                                            ?>
                                                <div class="footer__social">
                                                    <a href="<?= $socialIconLink['url'] ?>"><img class="style-svg" src="<?= $socialIcon["url"] ?>"></a>
                                                </div>
                                            <?php endwhile; ?>
                                        </li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                <?php endwhile; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if(have_rows("dark_bar", "option")): ?>
        <div class="footer__conditions-bar">
            <div class="footer__conditions-wrapper wrapper-full ">
                <div class="footer__m-wrapper d-flex d-flex-wrap">
                    <?php while(have_rows("dark_bar", "option")): the_row(); 
                    
                        $link = get_sub_field("link") ?  get_sub_field("link") : "";
                        ?>
    
                        <a class="footer__conditions text-regular" href="#"><?= $link["title"] ?></a>
    
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</footer>
	

<?php wp_footer(); ?>

</body>
</html>
