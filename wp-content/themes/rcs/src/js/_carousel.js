import $ from 'jquery';
import Slick from 'slick-carousel';

class Carousel {
    constructor() {

        this.init()
    }

    init() {
        $('.js-carousel').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            appendArrows: '.carousel__title_container .carousel__buttons',
            prevArrow: '.carousel__prev',
            nextArrow: '.carousel__next'
          });
    }
}

export default Carousel;