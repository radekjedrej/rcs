import $ from 'jquery'

class MobileMenu {
    constructor() {
        this.html = $("html");
        this.navbar = $(".nav-bar")
        this.burger = $(".nav-bar__burger-icon")
        this.menu = $(".nav-bar__mobile-dropdown")
        this.dropdown = $(".nav-bar__dropdown-list-item")
        this.events()
    }

    events() {
        this.burger.on("click", this.openMenu.bind(this))
        this.dropdown.on("click", this.openDropdown)
    }

    openMenu(e) {
        this.html.toggleClass("no-scroll")
        this.burger.toggleClass("active")
        this.navbar.toggleClass("drop")
        this.menu.toggleClass("open")
    }

    openDropdown(e) {

        const childDropdown = $(this).children(".nav-bar__second-dropdown");

        if($(this).find("ul.nav-bar__second-dropdown").length !== 0) {
            $(this).toggleClass("active")
            childDropdown.slideToggle()
        }
    }
}

export default MobileMenu