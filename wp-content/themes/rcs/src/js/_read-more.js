import $ from 'jquery'

class ReadMore {
    constructor() {
        this.readMore = $(".about__read-more");
        this.moreInfo = $(".about__more-info");
        this.events();
    }

    events() {
        this.readMore.on("click", this.showMore.bind(this))
    }

    showMore(e) {
        e.preventDefault();
        this.moreInfo.slideToggle();
        
        if (this.readMore.text() == "Read More") {
            this.readMore.text("Read Less")
        } else {
            this.readMore.text("Read More")
        }
    }
}

export default ReadMore