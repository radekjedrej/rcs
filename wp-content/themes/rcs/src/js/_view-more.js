import $ from 'jquery'

class ViewMore {
    constructor() {

        this.viewMoreCard = $(".cards__view-more")
        this.viewMoreGallery = $(".room-gallery__view-more")
        this.events();
    }

    events() {
        
        this.viewMoreCard.on("click", this.showMoreCards)
        this.viewMoreGallery.on("click", this.showMoreImages)
    }

    showMoreCards(e) {
        e.preventDefault();
        $(this).parent().children(".cards__box").slice(2).slideToggle();

        let viewMoreText = $(this).children(".more-button");

        if (viewMoreText.text() == "View More") {
            viewMoreText.text("View Less")
        } else {
            viewMoreText.text("View More")
        }
    }

    showMoreImages(e) {
        e.preventDefault();
        $(this).parent().children(".room-gallery__box").slice(1).slideToggle();

        let viewMoreText = $(this).children(".more-button");

        if (viewMoreText.text() == "View More") {
            viewMoreText.text("View Less")
        } else {
            viewMoreText.text("View More")
        }
    }
}

export default ViewMore