import './js/navigation';
import './js/skip-link-focus-fix';
import LazyLoad from 'vanilla-lazyload';
import Carousel from './js/_carousel';
import ReadMore from './js/_read-more';
import ViewMore from './js/_view-more';
import MobileMenu from './js/_mobile-menu';

import { vh, handleWindow } from './js/helpers'

window.addEventListener('DOMContentLoaded', () => {

    // Object Instance
    const myLazyLoad = new LazyLoad();

    new Carousel();
    new ReadMore();
    new ViewMore();
    new MobileMenu();
})

handleWindow();

