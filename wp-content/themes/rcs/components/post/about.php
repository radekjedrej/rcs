<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$info = get_sub_field("info") ? get_sub_field("info") : "";
$moreInfo = get_sub_field("info_read_more") ? get_sub_field("info_read_more") : "";
$sectionBreak = get_sub_field("section_break_line") ? get_sub_field("section_break_line") : "";

?>

<section class="about">
    <div class="about__row <?= $sectionBreak ?> d-flex">
        <div class="about__title-column">
            <h2 class="about__title text-button-regular color-green"><?= $title ?></h2>
        </div>
    
        <div class="about__info-column text-info">
            <div class="about__info"><?= $info ?></div>
            <div class="about__more-info"><?= $moreInfo ?></div>
            <a class="about__read-more color-green text-button-regular" href="#">Read More</a>
        </div>
    </div>
</section>