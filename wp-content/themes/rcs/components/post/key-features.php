<?php if(have_rows("features")): ?>
    <section class="key-features">
        <?php while(have_rows("features")): the_row(); 
        
            $title = get_sub_field("title") ? get_sub_field("title") : "";
            $sectionBreak = get_sub_field("section_break_line") ? get_sub_field("section_break_line") : "";
        
        ?>
            <div class="key-features__row <?= $sectionBreak ?> d-flex">
                <div class="key-features__title-col">
                    <h2 class="key-features__title text-button-regular color-green"><?= $title ?></h2>
                </div>
                <div class="key-features__features-col d-flex">
                    <?php if(have_rows("features_list_first_column")): ?>
                        <ul class="key-features__features-list text-info d-flex">
                            <?php while(have_rows("features_list_first_column")): the_row(); 
                            
                                $featureIcon = get_sub_field("feature_icon") ? get_sub_field("feature_icon") : "";
                                $featureTitle = get_sub_field("feature_title") ? get_sub_field("feature_title") : "";

                            ?>
                                <li class="key-features__feature d-flex">
                                    <img class="key-features__icon" src="<?=($featureIcon) ? $featureIcon['url'] : "" ?>">
                                    <span><?= $featureTitle ?></span>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>

                    <?php if(have_rows("features_list_second_column")): ?>
                        <ul class="key-features__features-list text-info d-flex">
                            <?php while(have_rows("features_list_second_column")): the_row(); 
                            
                                $featureIcon = get_sub_field("feature_icon") ? get_sub_field("feature_icon") : "";
                                $featureTitle = get_sub_field("feature_title") ? get_sub_field("feature_title") : "";

                            ?>
                                <li class="key-features__feature d-flex">
                                    <img class="key-features__icon" src="<?=($featureIcon) ? $featureIcon['url'] : "" ?>">
                                    <span><?= $featureTitle ?></span>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>