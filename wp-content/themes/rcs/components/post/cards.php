<?php if(have_rows("cards_row")): ?>
    <section class="cards">
        <?php while(have_rows("cards_row")): the_row(); 
        
            $count = count(get_sub_field("cards_repeater"));
            $title = get_sub_field("title") ? get_sub_field("title") : "";
            $sectionBreak = get_sub_field("section_break_line") ? get_sub_field("section_break_line") : "";
        
            ?>
            <div class="cards__row <?= $sectionBreak ?> d-flex d-flex-wrap">
                <div class="cards__title-col">
                    <h2 class="cards__title text-button-regular color-green"><?= $title ?></h2>
                </div>

                <?php if(have_rows("cards_repeater")): ?>
                    <div class="cards__cards-col d-flex d-flex-wrap">
                        <?php while(have_rows("cards_repeater")): the_row(); 
                        
                            $image = get_sub_field("image") ? get_sub_field("image") : "";
                            $cardTitle = get_sub_field("card_title") ? get_sub_field("card_title") : "";
                            $description = get_sub_field("card_description") ? get_sub_field("card_description") : "";
                            $buttonType = get_sub_field("button_type") ? get_sub_field("button_type") : "";

                            $buttonLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";
                            $buttonDownload = get_sub_field("button_download_link") ? get_sub_field("button_download_link") : "";

                            ?>
                            <div class="cards__box d-flex">
                                <div class="cards__img-container">
                                    <img class="cards__img" src="<?=($image) ? $image['url'] : "" ?>">
                                </div>
                                <div class="cards__content d-flex">
                                    <h3 class="cards__title text-button-regular color-green"><?= $cardTitle ?></h3>
                                    <p class="cards__copy text-info"><?= $description ?></p>
                                    <?php if($buttonType == 'link'): ?>
                                        <a href="<?= $buttonLink['url'] ?>" class="cards__btn cards__link u-btn u-btn--cards text-button-regular"><?= $buttonLink['title'] ?></a>
                                    <?php endif; ?>

                                    <?php if($buttonType == 'download'): ?>
                                        <a href="<?= $buttonDownload['url'] ?>" class="cards__btn cards__download u-btn u-btn--cards text-button-regular">
                                            <span class="cards__download-text">Download</span>
                                            <?php include get_icons_directory("arrow-down-to-line.svg") ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php if($count > 2): ?>
                            <a href="#" class="cards__view-more d-flex">
                                <div class="cards__view-more-text more-button text-button-regular color-green">View More</div>
                                <?php include get_icons_directory("arrow-down.svg") ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>