<?php if(have_rows("location_row")): ?>
    <section class="location">
        <?php while(have_rows("location_row")): the_row(); 
    
            $title = get_sub_field("title") ? get_sub_field("title") : "";
            $sectionBreak = get_sub_field("section_break_line") ? get_sub_field("section_break_line") : "";
            $mapEmbedLink = get_sub_field("map_embed_link") ? get_sub_field("map_embed_link") : "";
            $googleMapLink = get_sub_field("google_maps_link") ? get_sub_field("google_maps_link") : "";  
            ?>

            <div class="location__row <?= $sectionBreak ?> d-flex d-flex-wrap">
                <div class="location__header">
                    <h2 class="location__title text-button-regular color-green"><?= $title ?></h2>
                </div>

                    <div class="location__map d-flex d-flex-wrap">
                         <div class="location__box d-flex">
                            <div class="location__container">
                                <iframe class="location__iframe" src="<?= $mapEmbedLink ?>" width="600" height="450" style="border:0; -webkit-filter: grayscale(100%); filter: grayscale(100%);" allowfullscreen="" loading="lazy"></iframe>
                                <a class="location__link color-green" href="<?= $googleMapLink ?>">
                                    <span class="location__text">Open in Maps</span>
                                    <?php include get_icons_directory("arrow-right.svg") ?> 
                                </a>
                            </div>
                        </div>
                    </div>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>