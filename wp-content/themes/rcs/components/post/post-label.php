<section class="post-label d-desktop">
    <div class="post-label__label d-flex">
        <div class="post-label__title-container d-flex">
            <h1 class="post-label__post-title text-subheader"><?= get_the_title(); ?></h1>
        </div>
        <?php if(have_rows("icons_row")): ?>
            <div class="post-label__icons-row d-flex">
                <?php while(have_rows("icons_row")): the_row();
                    
                    $icon = get_sub_field("icon") ? get_sub_field("icon") : "";

                    $svg = $icon ? $icon['url'] : "";

                    $iconLabel = get_sub_field("icon_label") ? get_sub_field("icon_label") : "";
                    
                    ?>
                    <div class="post-label__icon-column">
                        <img class="post-label__icon" src="<?= $svg ?>">
                        <div class="post-label__icon-label text-icon"><?= $iconLabel ?></div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>