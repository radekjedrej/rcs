<?php if(have_rows("gallery")): ?>
    <section class="room-gallery">
        <?php while(have_rows("gallery")): the_row(); 
    
            $count = count(get_sub_field("gallery_repeater"));
            $title = get_sub_field("title") ? get_sub_field("title") : "";
            $sectionBreak = get_sub_field("section_break_line") ? get_sub_field("section_break_line") : "";
        
            ?>
            <div class="room-gallery__row <?= $sectionBreak ?> d-flex d-flex-wrap">
                <div class="room-gallery__title-col">
                    <h2 class="room-gallery__title text-button-regular color-green"><?= $title ?></h2>
                </div>

                <?php if(have_rows("gallery_repeater")): ?>
                    <div class="room-gallery__gallery-col d-flex d-flex-wrap">
                        <?php while(have_rows("gallery_repeater")): the_row(); 
                        
                            $image = get_sub_field("image") ? get_sub_field("image") : "";
                            $description = get_sub_field("description") ? get_sub_field("description") : "";
                            ?>
                            <div class="room-gallery__box d-flex">
                                <div class="room-gallery__img-container">
                                    <img class="room-gallery__img" src="<?=($image) ? $image['url'] : "" ?>">
                                </div>
                                <div class="room-gallery__content d-flex">
                                    <p class="room-gallery__copy text-info"><?= $description ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php if($count > 1): ?>
                            <a href="#" class="room-gallery__view-more d-flex">
                                <div class="room-gallery__view-more-text more-button text-button-regular color-green">View More</div>
                                <?php include get_icons_directory("arrow-down.svg") ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>