<?php

$boxesSide = get_sub_field("box_grid_side");
$background = get_sub_field("gray_background");
$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";

$btnLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";

$altBtnType = get_sub_field("alternate_button_type") ? get_sub_field("alternate_button_type") : "";
$altBtnLink = get_sub_field("alternate_button_link") ? get_sub_field("alternate_button_link") : "";
$altBtnDownload = get_sub_field("alternate_button_download") ? get_sub_field("alternate_button_download") : "";
$altBtnIcon =  get_sub_field("alternate_button_icon") ? get_sub_field("alternate_button_icon") : "";

?>

<!-- FEATURES INFOGRAPHIC - START -->
<section class="features-infographic <?=($background) ? "features-infographic--gray" : "" ?> wrapper-stretched">
    <div class="features-infographic__row <?= $boxesSide ?> wrapper-features d-flex d-flex-wrap">
        <div class="features-infographic__content-box d-flex">
            <div class="features-infographic__content">
                <h2 class="features-infographic__title text-header text__line"><?= $title ?></h1>
                <p class="features-infographic__copy text-regular"><?= $text ?></p>
                
                <?php if($btnLink OR $altBtnType): ?>
                    <div class="features-infographic__button-row d-flex d-flex-wrap">
    
                    <?php if($btnLink): ?>
                        <a class="features-infographic__button u-btn u-btn--transition" href="<?= $btnLink['url'] ?>">
                            <div class="u-btn__hover-div"></div>
                            <span class="u-btn__text text-button-regular"><?= $btnLink['title'] ?></span>
                            <?php include get_icons_directory("arrow-right.svg") ?>
                        </a>
                                   
                    <?php endif; ?>
                        
                    <?php if($altBtnType): ?>
                        <a class="features-inforaphic__alt-button u-btn u-btn--transparent u-btn--transparent-green text-button-regular" href="<?=($altBtnType == 'link') ? $altBtnLink['url'] : $altBtnDownload['url'] ?>">
                            <?=($altBtnType == 'link') ? $altBtnLink['title'] : "Download" ?>
                            <?php if($altBtnIcon): ?>
                                <img class="u-btn__icon style-svg" src="<?= $altBtnIcon['url'] ?>">  
                            <?php endif; ?>   
                        </a>
                    <?php endif; ?>
                    </div>
    
                <?php endif; ?>
            </div>
        </div>

        <div class="features-infographic__grid-box d-flex d-flex-wrap">
        <?php if(have_rows("feature_boxes")): ?>
            <?php while(have_rows("feature_boxes")): the_row();
            
                $boxType = get_sub_field("box_type");
                $boxTitle = get_sub_field("box_title") ? get_sub_field("box_title") : "";
                $boxText = get_sub_field("box_text") ? get_sub_field("box_text") : "";
            
            ?>
                <div class="features-infographic__item <?= $boxType ?>">
                    <div class="features-infographic__box-inner d-flex">
                        <?php if(have_rows("icons")): ?>
                            <div class="features-infographic__box-icons">
                                <?php while(have_rows("icons")): the_row(); 
                                
                                    $icon = get_sub_field("icon");
                                    $svg = $icon['url'];

                                ?>
                                    <?php if($svg): ?>
                                    <img src="<?= $svg ?>">
                                    <?php endif; ?>

                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <div class="features-infographic__box-content">
                            <h3 class="features-infographic__box-title text-subheader color-white"><?= $boxTitle ?></h1>
                            <p class="features-infographic__box-info text-info color-white"><?= $boxText ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>

    </div>
</section>
<!-- FEATURES INFOGRAPHIC - END -->