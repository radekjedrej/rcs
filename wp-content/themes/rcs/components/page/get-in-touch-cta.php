<?php

$bgImage = get_sub_field("background_image") ? get_sub_field("background_image") : "";
$picture = $bgImage['url'] ? $bgImage['url'] : "";

$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$btnLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";
$altBtnLink = get_sub_field("alternate_button_link") ? get_sub_field("alternate_button_link") : "";
$altBtnIcon =   get_sub_field("alternate_button_icon") ? get_sub_field("alternate_button_icon") : "";


?>

<!-- GET IN TOUCH CTA - START -->
<section class="get-in-touch-cta">
    <div class="get-in-touch-cta__inner">
        <div class="get-in-touch-cta__banner">
            <img class="get-in-touch-cta__bgImage" src="<?= $picture ?>" alt="CTA Background Image">
            <div class="get-in-touch-cta__content wrapper-xfull">
                <h1 class="get-in-touch-cta__title text-header color-white text__line text__line--white"><?= $title ?></h1>
                <p class="get-in-touch-cta__copy text-banner-copy color-white"><?= $text ?></p>
                <div class="get-in-touch-cta__button-row d-flex d-flex-wrap">
                
                <?php if($btnLink): ?>
                    <a class="get-in-touch-cta__button u-btn u-btn--transition u-btn--white" href="<?= $btnLink['url'] ?>">
                        <div class="u-btn__hover-div"></div>
                        <span class="u-btn__text text-button-regular"><?= $btnLink['title'] ?></span>
                        <?php include get_icons_directory("arrow-right.svg") ?>
                    </a>
                <?php endif; ?>
                        
                <?php if($altBtnLink): ?>
                    <a class="get-in-touch-cta__alt-button u-btn u-btn--transparent u-btn--transparent-white text-button-regular" href="<?= $altBtnLink['url'] ?>">
                        <?= $altBtnLink['title'] ?>
                        <?php if($altBtnIcon): ?>
                            <img class="u-btn__icon style-svg" src="<?= $altBtnIcon['url'] ?>">  
                        <?php endif; ?>
                    </a>        
                <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- GET IN TOUCH CTA - END -->