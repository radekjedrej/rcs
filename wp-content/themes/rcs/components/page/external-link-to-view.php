<?php

    $grayBg = get_sub_field("gray_background") ? get_sub_field("gray_background") : "";
    $content = get_sub_field("content") ? get_sub_field("content") : "";


?>

<section class="external-link-to-view wrapper-stretched d-flex <?=($grayBg) ? "external-link-to-view--gray-bg" : "" ?>">
    <div class="external-link-to-view__wrapper"><?= $content ?></div>
</section>