<?php

$background = get_sub_field('spacing_background');
$space = get_sub_field('spacing');

?>

<!-- SPACING ROW - START -->
<div class="spacing-row <?= $background ?> space-<?= $space ?> mobile"></div>
<!-- SPACING ROW - END -->
