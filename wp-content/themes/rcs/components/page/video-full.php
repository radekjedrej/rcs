<?php

$video = get_sub_field("video") ? get_sub_field("video") : "";

?>

<!-- VIDEO FULL - START -->
<section class="video-full wrapper-stretched">
    <div class="video-full__responsive-media"><?= $video ?></div>
</section>
<!-- VIDEO FULL - END -->