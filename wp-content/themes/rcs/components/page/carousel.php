<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
                
?>

<section class="carousel wrapper-stretched">
    <div class="carousel__title_container wrapper-full d-flex">
        <h2 class="carousel__title text-header text__line"><?= $title ?></h2>
        <div class="carousel__buttons d-flex">
            <div class="carousel__prev slick-prev"><img src="<?= get_template_directory_uri()?>/src/images/svg/left-chevron.svg"></div>
            <div class="carousel__next slick-next"><img src="<?= get_template_directory_uri()?>/src/images/svg/right-chevron.svg"></div>
        </div>
    </div>
    <?php if(have_rows("carousel")): ?>
        <div class="carousel__container js-carousel">
            <?php while(have_rows("carousel")): the_row(); 
            
                $image = get_sub_field("image") ? get_sub_field("image") : "";
                $picture = $image['url'] ? $image['url'] : "";

                $imageTitle = get_sub_field("optional_image_title") ? get_sub_field("optional_image_title") : "";
            
            ?>
                <div class="carousel__slide-container">
                    <div class="carousel__optional-title-container">
                        <h3 class="carousel__optional-title text-subheader"><?= $imageTitle ?></h3>
                    </div>
                    <img class="carousel__image img-fluid" src="<?= $picture ?>" alt="Carousel image">
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</section>