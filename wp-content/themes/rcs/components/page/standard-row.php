<?php 

$background = get_sub_field("gray_background");
$image = get_sub_field("image") ? get_sub_field("image") : "";
$picture = $image['url'] ? $image['url'] : "";
    
$imageSide = get_sub_field("image_side");
$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$btnLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";
$altBtnType = get_sub_field("alternate_button_type") ? get_sub_field("alternate_button_type") : "";
$altBtnLink = get_sub_field("alternate_button_link") ? get_sub_field("alternate_button_link") : "";
$altBtnDownload = get_sub_field("alternate_button_download") ? get_sub_field("alternate_button_download") : "";
$altBtnIcon =   get_sub_field("alternate_button_icon") ? get_sub_field("alternate_button_icon") : "";

?>

<!-- STANDARD ROW - START -->
<div class="standard-row <?=($background) ? "standard-row--gray" : "" ?>">
    <section class="standard-row__wrapper wrapper-xfull">
        <div class="standard-row__inner">
            <div class="standard-row__background">
                <div class="standard-row__row <?=($imageSide) ? $imageSide : "" ?> d-flex d-flex-wrap wrapper-full">
                    <div class="standard-row__img-container">
                        <img class="standard-row__img lazy" data-src="<?= $picture ?>" alt="Standard row image">  
                    </div>
                    <div class="standard-row__content-container d-flex">
                        <div class="standard-row__content">
        
                        <?php if($title): ?>
                            <h2 class="standard-row__text-header text-header text__line"><?= $title ?></h1>
                        <?php endif; ?>
        
                        <?php if($text): ?>
                            <p class="standard-row__copy text-regular"><?= $text ?></p>
                        <?php endif; ?>
        
                            <div class="standard-row__button-row d-flex d-flex-wrap">
                            <?php if($btnLink): ?>
                                <a class="standard-row__button u-btn u-btn--transition mr-lg-1" href="<?= $btnLink['url'] ?>">
                                    <div class="u-btn__hover-div"></div>
                                    <span class="u-btn__text text-button-regular"><?= $btnLink['title'] ?></span>
                                    <?php include get_icons_directory("arrow-right.svg") ?>
                                </a>    
                            <?php endif; ?>
                                
                            <?php if($altBtnType): ?>
                                <a class="standard-row__alt-button u-btn u-btn--transparent u-btn--transparent-green text-button-regular" href="<?=($altBtnType == 'link') ? $altBtnLink['url'] : $altBtnDownload['url'] ?>">
                                    <?=($altBtnType == 'link') ? $altBtnLink['title'] : "Download" ?>
                                    <?php if($altBtnIcon): ?>
                                        <img class="u-btn__icon style-svg" src="<?= $altBtnIcon['url'] ?>">  
                                    <?php endif; ?>
                                </a>
                                
                            <?php endif; ?>
        
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- STANDARD ROW - END -->