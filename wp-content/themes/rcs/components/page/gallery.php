<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";

?>

<section class="gallery wrapper-full">
    <h2 class="gallery__title text-header text__line"><?= $title ?></h2>
    <?php if(have_rows("gallery")): ?>
    <div class="gallery__row d-flex d-flex-wrap">
        <?php while(have_rows("gallery")): the_row();
            
            $image = get_sub_field("image") ? get_sub_field("image") : "";
            $picture = $image['url'] ? $image['url'] : "";
            
        ?>
            <div class="gallery__box">
                <img class="gallery__img" src="<?= $picture ?>" alt="Gallery image">
            </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</section>