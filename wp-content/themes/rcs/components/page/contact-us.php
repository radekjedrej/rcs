<?php

$sectionTitle = get_sub_field("section_title") ? get_sub_field("section_title") : "";
$googleMap = get_sub_field("google_map_embed_link") ? get_sub_field("google_map_embed_link") : "";
$contactForm = get_sub_field("contact_form") ? get_sub_field("contact_form") : "";
$formTitle = get_sub_field("form_title") ? get_sub_field("form_title") : "";

?>

<section class="contact-us">
    <div class="<?=(wp_is_mobile()) ? "wrapper-stretched" : "wrapper-full" ?>">
        <div class="contact-us__row d-flex d-flex-wrap">
            <div class="contact-us__contact-info d-flex d-flex-wrap">
                <div class="contact-us__title-container d-flex">
                    <h2 class="contact-us__title text-subheader"><?= $sectionTitle ?></h2>
                </div>
                <?php if(have_rows("informations")): ?>
                    <div class="contact-us__info-rows d-flex d-flex-wrap">
                        <?php while(have_rows("informations")): the_row();
                            
                            $title = get_sub_field("title_optional") ? get_sub_field("title_optional") : "";
                            $text = get_sub_field("text") ? get_sub_field("text") : "";
                            
                            ?>
                            <div class="contact-us__info d-flex">
                                <div class="contact-us__info-content">
                                    <?php if($title): ?>
                                        <h3 class="contact-us__info-title text-button"><?= $title ?></h3>
                                    <?php endif; ?>
                                    
                                    <?php if($text): ?>
                                        <p class="contact-us__copy text-regular"><?= $text ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <iframe class="contact-us__gmap" src="<?= $googleMap ?>" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                <?php endif; ?>
            </div>
            <?php if($contactForm): ?>
                <div class="contact-us__form-column">
                    <div class="contact-us__title-container contact-us__title-container--form-title d-flex">
                        <h2 class="contact-us__title text-subheader"><?= $formTitle ?></h2>
                    </div>
                    <form class="contact-us__form d-flex d-flex-wrap">
                        <input class="contact-us__input contact-us__input--half text-info" type="text" placeholder="First Name">
                        <input class="contact-us__input contact-us__input--half text-info" type="text" placeholder="Last Name">
                        <input class="contact-us__input contact-us__input--full text-info" type="email" placeholder="Email">
                        <input class="contact-us__input contact-us__input--full text-info" type="tel" placeholder="Phone Number">
                        <textarea class="contact-us__input contact-us__input--full contact-us__textarea text-info" placeholder="Your message, or please let us know the best time for us to call you."></textarea>
                        <div>
                            <p class="color-gray text-button">How we use your data</p>
                            <div class="d-flex mb-2">
                                <input type="checkbox">
                                <div class="contact-us__checkbox-description color-gray text-info">I have read and agree to your <a href="#" class="color-green">terms and conditions</a> and understand how my data will be used as outlined in your <a href="#" class="color-green">privacy policy</a></div>
                            </div>
                            <a class="contact-us__btn u-btn text-button" href="#">Enquire</a>
                            
                        </div>
                    </form>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>