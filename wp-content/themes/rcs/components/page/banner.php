<?php

$bgImage = get_sub_field("background_image") ? get_sub_field("background_image") : "";
$picture = $bgImage['url'] ? $bgImage['url'] : "";
$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$btnLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";
                
?>

<!-- BANNER - START -->
<section class="banner wrapper-stretched">
    <div class="banner__inner">
        <img class="banner__bg-image" src="<?= $picture ?>" alt="Banner image">
        <div class="banner-small__content wrapper-full">
            <div class="banner__container">
                
                <div class="banner__content">
                    <h1 class="banner__title text-header color-white text__line text__line--white"><?= $title ?></h1>
                    <p class="banner__copy text-regular color-white"><?= $text ?></p>
                    
                    <?php if($btnLink): ?>
                        <a class="banner__button u-btn" href="<?= $btnLink['url'] ?>">
                            <div class="u-btn__hover-div"></div>
                            <span class="u-btn__text text-button-regular"><?= $btnLink['title'] ?></span>
                        </a>            
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="banner__dark"></div>
    </div>
</section>
<!-- BANNER - END -->