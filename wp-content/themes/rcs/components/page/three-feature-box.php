<?php
    
$title = get_sub_field("title") ? get_sub_field("title") : "";

?>

<!-- THREE FEATURE BOX - START -->
<section class="three-feature-box wrapper-full">
    <div class="three-feature-box__title-container">
        <h2 class="three-feature-box__title text__line text-header">Our available spaces<br>in the heart of London</h1>
    </div>
    <?php if(have_rows("features")): ?>
    <div class="three-feature-box__row d-flex d-flex-wrap">
        <?php while(have_rows("features")): the_row(); 
        
            $image = get_sub_field("image") ? get_sub_field("image") : "";
            $picture = $image['url'] ? $image['url'] : "";
            
            $featureTitle = get_sub_field("feature_title") ? get_sub_field("feature_title") : "";
            $text = get_sub_field("text") ? get_sub_field("text") : "";
            $btnLink = get_sub_field("button_link") ? get_sub_field("button_link") : "";            
        ?>

        <div class="three-feature-box__box">
            <div class="three-feature-box__img-box">
                <img class="three-feature-box__img lazy" data-src="<?= $picture ?>" alt="Feature image">
            </div>
            <div class="three-feature-box__content">
                <div class="three-feature-box__content-inner">
                    <?php if($featureTitle): ?>
                        <h3 class="text-subheader"><?= $featureTitle ?></h2>
                    <?php endif; ?>
    
                    <?php if($text): ?>
                        <p class="three-feature-box__copy text-regular"><?= $text ?></p>
                    <?php endif; ?>
    
                    <?php if($btnLink): ?>
                    <a class="three-feature-box__button u-btn" href="<?= $btnLink['url'] ?>">
                        <div class="u-btn__hover-div"></div>
                        <span class="u-btn__text text-button-regular"><?= $btnLink['title'] ?></span>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</section>
<!-- THREE FEATURE BOX - END -->