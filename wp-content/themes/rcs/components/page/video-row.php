<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$video = get_sub_field("video") ? get_sub_field("video") : "";

?>

<!-- VIDEO ROW - START -->
<section class="video-row wrapper-full d-flex d-flex-wrap">
    <div class="video-row__content-box d-flex d-flex-wrap">
        <div class="video-row__content">
            <h2 class="video-row__title text-header text__line"><?= $title ?></h2>
            <p class="video-row__copy text-regular"><?= $text ?></p>
        </div>
    </div>
    <div class="video-row__video-box">
        <div class="video-row__responsive-media"><?= $video ?></div>
    </div>
</section>
<!-- VIDEO ROW - END -->