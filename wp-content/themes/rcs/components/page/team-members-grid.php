<?php

$grayBg = get_sub_field("gray_background") ? get_sub_field("gray_background") : "";
$title = get_sub_field("title") ? get_sub_field("title") : "";
$description = get_sub_field("description") ? get_sub_field("description") : "";

?>

<section class="team-members-grid wrapper-stretched <?=($grayBg) ? "team-members-grid--gray" : "" ?>">
    <div class="team-members-grid__row wrapper-full d-flex d-flex-wrap">
        
        <div class="team-members-grid__info-column">
            <div class="team-members-grid__info-content">
                <h2 class="team-members-grid__section-title text-header text__line"><?= $title ?></h2>
                <p class="team-members-grid__info text-regular"><?= $description ?></p>
            </div>
        </div>
        
        <?php if(have_rows("members")): ?>
            <div class="team-members-grid__members-grid d-flex d-flex-wrap">
            <?php while(have_rows("members")): the_row();
                
                $image = get_sub_field("image") ? get_sub_field("image") : "";
                $picture = $image['url'] ? $image['url'] : "";

                $name = get_sub_field("name") ? get_sub_field("name") : "";
                $jobTitle = get_sub_field("job_title") ? get_sub_field("job_title") : "";
                $copy = get_sub_field("copy") ? get_sub_field("copy") : ""; 
                
                ?>
                <div class="team-members-grid__member">
                    <div class="team-members-grid__img-container">
                        <img class="team-members-grid__img" src="<?= $picture ?>" alt="Member Image">
                    </div>
                    <div class="team-members-grid__content-container">
                        <div class="team-members-grid__member-content">
                            <h3 class="team-members-grid__name text-subheader"><?= $name ?></h3>
                            <h4 class="team-members-grid__job-title text-button"><?= $jobTitle ?></h4>
                            <p class="team-members-grid__copy text-regular"><?= $copy ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>