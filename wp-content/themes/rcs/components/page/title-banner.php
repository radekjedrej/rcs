<?php

$bgImage = get_sub_field("background_image") ? get_sub_field("background_image") : "";
$picture = $bgImage['url'] ? $bgImage['url'] : "";
$title = get_sub_field("title") ? get_sub_field("title") : "";

?>

<section class="title-banner wrapper-stretched">
    <div class="title-banner__img-dark"></div>
    <img class="title-banner__img" src="<?= $picture ?>">
    <div class="title-banner__title-container">
        <h1 class="title-banner__title color-white text__line text__line--white"><?= $title ?></h1>
    </div>
    
</section>