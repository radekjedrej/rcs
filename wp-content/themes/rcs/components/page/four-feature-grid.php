<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$imageGrid = get_sub_field("image_grid") ? get_sub_field("image_grid") : "";
$imageGridSide = get_sub_field("image_grid_side");

?>

<section class="four-feature-grid wrapper-full">
    <h2 class="four-feature-grid__title text-header text__line"><?= $title ?></h1>
    <div class="four-feature-grid__row <?= $imageGridSide ?> d-flex d-flex-wrap">
        <?php if($imageGrid):
            
            $bigImage = $imageGrid["big_image"] ? $imageGrid["big_image"] : "";
            $smallImage1 = $imageGrid["small_image_first"] ? $imageGrid["small_image_first"] : "";
            $smallImage2 = $imageGrid["small_image_second"] ? $imageGrid["small_image_second"] : "";
            $longImage = $imageGrid["long_image"] ? $imageGrid["long_image"] : "";

        ?>   
            <div class="four-feature-grid__box four-feature-grid__big-image">
                <?php if($bigImage): 
                    
                    $image = $bigImage["image"] ? $bigImage["image"] : "";
                    $imageTitle = $bigImage["image_title"] ? $bigImage["image_title"] : "";
                    $picture = $image['url'] ? $image['url'] : ""; 
                    
                ?>
                    <div class="four-feature-grid__image-half">
                        <div class="four-feature-grid__image-title">
                            <h3 class="text-subheader color-white text__line text__line--white"><?= $imageTitle ?></h2>
                        </div>
                        <img class="four-feature-grid__image lazy" data-src="<?= $picture ?>" alt="Big Image">
                    </div>
                <?php endif; ?>
            </div>
            <div class="four-feature-grid__box four-feature-grid__grid d-flex d-flex-wrap">
                <?php if($smallImage1): 
                    
                    $image = $smallImage1["image"] ? $smallImage1["image"] : "";
                    $imageTitle = $smallImage1["image_title"] ? $smallImage1["image_title"] : "";

                    if($image):
                        $picture = $image['url'];
                
                    else:
                        $picture = "";
                
                    endif;
                    
                ?>
                    <div class="four-feature-grid__image-small">
                        <div class="four-feature-grid__image-title">
                            <h2 class="text-subheader color-white text__line text__line--white"><?= $imageTitle ?></h2>
                        </div>
                        <img class="four-feature-grid__image lazy" data-src="<?= $picture ?>" alt="First small image">
                    </div>
                <?php endif; ?>

                <?php if($smallImage2): 
                    
                    $image = $smallImage2["image"] ? $smallImage2["image"] : "";
                    $imageTitle = $smallImage2["image_title"] ? $smallImage2["image_title"] : "";

                    if($image):
                        $picture = $image['url'];
    
                    else:
                        $picture = "";
                
                    endif;
                    
                ?>
                    <div class="four-feature-grid__image-small">
                        <div class="four-feature-grid__image-title">
                            <h2 class="text-subheader color-white text__line text__line--white"><?= $imageTitle ?></h2>
                        </div>
                        <img class="four-feature-grid__image lazy" data-src="<?= $picture ?>" alt="Second small image">
                    </div>
                <?php endif; ?>

                <?php if($longImage): 
                    
                    $image = $longImage["image"] ? $longImage["image"] : "";
                    $imageTitle = $longImage["image_title"] ? $longImage["image_title"] : "";

                    if($image):
                        $picture = $image['url'];
    
                    else:
                        $picture = "";
                
                    endif;
                    
                ?>
                    <div class="four-feature-grid__image-long">
                        <div class="four-feature-grid__image-title">
                            <h2 class="text-subheader color-white text__line text__line--white"><?= $imageTitle ?></h2>
                        </div>
                        <img class="four-feature-grid__image lazy" data-src="<?= $picture ?>">    
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</section>