<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header();

$bigImage = get_field("big_image") ? get_field("big_image") : "";
$smallImage1 = get_field("small_image_first") ? get_field("small_image_first") : "";
$smallImage2 = get_field("small_image_second") ? get_field("small_image_second") : "";
$longImage = get_field("long_image") ? get_field("long_image") : "";

?>

	<div id="primary" class="content-area">
		<main id="main" class="main-wrapper">
			<section class="image-grid wrapper-stretched">
				<div class="image-grid__row d-flex">
					<div class="image-grid__box d-flex">
						<div class="image-grid__big-image">
							<img class="image-grid__image" src="<?=($bigImage) ? $bigImage['url'] : "" ?>">
						</div>
					</div>
					<div class="image-grid__box d-flex d-flex-wrap">
						<div class="image-grid__small-image">
							<img class="image-grid__image" src="<?=($smallImage1) ? $smallImage1['url'] : "" ?>">
						</div>
						<div class="image-grid__small-image">
							<img class="image-grid__image" src="<?=($smallImage2) ? $smallImage2['url'] : "" ?>">
						</div>
						<div class="image-grid__long-image">
							<img class="image-grid__image" src="<?=($longImage) ? $longImage['url'] : "" ?>">
						</div>
					</div>
				</div>
			</section>
			<div class="wrapper-full">
				<div class="post-content">
					<?php 
					while ( have_posts() ): the_post(); 
		
						rcs_layout("post_partials", "post");

					endwhile; // End of the loop. 
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
