<?php
/**
 * Custom post types tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _s
 */


 /**
 * Register custom fields Global Options
 */

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Main Menu',
		'menu_title'	=> 'Main Menu',
		'menu_slug' 	=> 'menu-settings',
		'capability'	=> 'edit_posts',
        'icon_url'      => 'dashicons-editor-justify',
		'redirect'		=> true
	));

	acf_add_options_page(array(
		'page_title' 	=> '404 Settings',
		'menu_title'	=> '404 Settings',
		'menu_slug'	=> '404-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug'	=> 'footer-settings',
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Navigation Links',
	// 	'menu_title'	=> 'Navigation Links',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Marketing Settings',
	// 	'menu_title'	=> 'Marketing',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Archives Settings',
	// 	'menu_title'	=> 'Archives',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Forms Settings',
	// 	'menu_title'	=> 'Forms',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

}

 /**
 * Register post types
 */

function rcs_custom_post_types() {
    register_post_type('rooms', array(
		'show_in_rest' => true,
        'supports' => array('title', 'editor'),
        'labels' => array(
            'name' => 'Rooms',
            'singular_name' => 'Room',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Room',
			'new_item' => 'New Room',
			'edit_item' => 'Edit Room',
			'all_items' => 'All Rooms',
			'view_item' => 'View Room'
            ),
        'public'      => true,
        'has_archive' => true,
		'menu_icon' => 'dashicons-admin-home'
    ));
}

add_action('init', 'rcs_custom_post_types');